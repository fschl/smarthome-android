package local.oems_g5_projekt;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;

import android.util.Patterns;

public class MainActivity extends AppCompatActivity implements ServiceConnection, MQTTServiceCallbacks {

    private MQTTService mqttService;
    private Button btnConnect;
    private TextView txtTempLabel;
    private TextView txtTempOut;
    private CheckBox chkLed1;
    private CheckBox chkLed2;
    private CheckBox chkLed3;
    private CheckBox chkTemp;
    private ImageView imgLed1;
    private ImageView imgLed2;
    private ImageView imgLed3;
    private EditText ipInput;
    private String ip;
    private SharedPreferences settings;
    private ArrayList<ImageView> imageList;

    /**
     * Lifecycle method from this activity, is called when this activity is created.
     * Initializes view,GUI components,CallbackHandler,ServiceHelper and action event for Button btnConnect.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_portrait);
        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Log.i("App_status", "Created.");
        initializeOnSwitch();
    }

    /**
     * Initializes all checkboxes, buttons, etc.
     */
    private void initializeOnSwitch() {
        /*
        May not be called only in the onCreate method, since after changing to another layout, those R.id.~ are no longer valid.
         */
        btnConnect = (Button) findViewById(R.id.btnConnect);
        txtTempLabel = (TextView) findViewById(R.id.txtLed2);
        txtTempOut = (TextView) findViewById(R.id.txtTempOut);

        chkLed1 = (CheckBox) findViewById(R.id.chkLed1);
        chkLed2 = (CheckBox) findViewById(R.id.chkLed2);
        chkLed3 = (CheckBox) findViewById(R.id.chkLed3);
        chkTemp = (CheckBox) findViewById(R.id.chkTemp);

        ipInput = (EditText) findViewById(R.id.ipInput);

        imgLed1 = (ImageView) findViewById(R.id.imgLed1);
        imgLed2 = (ImageView) findViewById(R.id.imgLed2);
        imgLed3 = (ImageView) findViewById(R.id.imgLed3);

        chkLed1.setChecked(getCheckBoxSettings("chk1"));
        chkLed2.setChecked(getCheckBoxSettings("chk2"));
        chkLed3.setChecked(getCheckBoxSettings("chk3"));
        chkTemp.setChecked(getCheckBoxSettings("chkt"));

        changeLedStatus(chkLed1.isChecked(), imgLed1);
        changeLedStatus(chkLed2.isChecked(), imgLed2);
        changeLedStatus(chkLed3.isChecked(), imgLed3);

        Log.i("App_status", "Setting listeners...");
        setClickListener();

        restoreIp();
        checkIpInput();

        setConnectListener(btnConnect);

        setChangedListener(chkLed1, "chk1", imgLed1);
        setChangedListener(chkLed2, "chk2", imgLed2);
        setChangedListener(chkLed3, "chk3", imgLed3);

        setChangedListener(chkTemp, "chkt");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("App_status", "Resumed.");
        //initializeOnSwitch();
        Intent intent = new Intent(this, MQTTService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    private void setConnectListener(Button button) {
        /*button.setOnClickListener((buttonView)->{
            connectToMqtt(ip);
        });*/
    }

    /**
     * Restores the IP
     */
    private void restoreIp() {
        boolean ipChecksOut = settings.getBoolean("ip", false);
        btnConnect.setEnabled(ipChecksOut);
        ipInput.setText(settings.getString("ipadress", "127.0.0.1"));
        if (ipChecksOut)
            btnConnect.setBackgroundResource(android.R.drawable.button_onoff_indicator_on);
        else
            btnConnect.setBackgroundResource(android.R.drawable.button_onoff_indicator_off);
        Log.i("App_status", "Restoring ip adress " + settings.getString("ipadress", "[missing]") + ", button enabled =" + ipChecksOut);
    }

    /**
     * Returns the persistent state of a checkbox
     *
     * @param chkBoxId
     * @return
     */
    private boolean getCheckBoxSettings(String chkBoxId) {
        return settings.getBoolean(chkBoxId, false);
    }

    /**
     * Persist the current state of a checkbox
     *
     * @param chkBoxId
     * @param chkBox
     */
    private void setCheckBoxSettings(String chkBoxId, CheckBox chkBox) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(chkBoxId, chkBox.isChecked());
        editor.commit();
    }

    /**
     * Is called when screen is flipped.
     * Changes view from normal to flipped and vice versa.
     *
     * @param config
     */
    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main_portrait);
            Log.i("App_status", "Switched to portrait");
            initializeOnSwitch();

        } else {
            setContentView(R.layout.activity_main_landscape);
            Log.i("App_status", "Switched to landscape");
            initializeOnSwitch();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        MQTTServiceBinder b = (MQTTServiceBinder) binder;
        mqttService = b.getService();
        mqttService.registerClient(this);
        Toast.makeText(MainActivity.this, "Service started...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mqttService = null;
    }

    /**
     * Changes the status of a LED image
     *
     * @param isChecked
     * @param img
     */
    private void changeLedStatus(boolean isChecked, ImageView img) {
        // TODO 3) switch LED images in View, Hint: check resources files
        if (isChecked) {
            if (img == imgLed1)
                img.setImageResource(R.drawable.red_led_on);
        } else {
            if (img == imgLed1)
                img.setImageResource(R.drawable.red_led_off);
        }
    }

    /**
     * Validates the Input of the IP. If coorect, the IP can be used to connect to MQTT.
     */
    private void checkIpInput() {
        ipInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            /**
             * If the given String is in the ip-format, persist it.
             * In either case, set the button enabled/disabled, change its image accordingly and persist the button state.
             * @param s
             */
            @Override
            public void afterTextChanged(Editable s) {
                Matcher matcher = Patterns.IP_ADDRESS.matcher(s);
                boolean ipChecksOut;
                SharedPreferences.Editor editor = settings.edit();
                if (matcher.matches()) {
                    ip = s.toString();
                    editor.putString("ipadress", ip);
                    ipChecksOut = true;
                    btnConnect.setEnabled(true);
                    btnConnect.setBackgroundResource(android.R.drawable.button_onoff_indicator_on);
                } else {
                    if (btnConnect.isEnabled())
                        btnConnect.setEnabled(false);
                    editor.putString("ipadress", s.toString());
                    ipChecksOut = false;
                    btnConnect.setBackgroundResource(android.R.drawable.button_onoff_indicator_off);
                }
                editor.putBoolean("ip", ipChecksOut);
                Log.i("App_status", "Button enabled = " + ipChecksOut + " for ip adress " + ip);
                editor.commit();

            }
        });
    }

    /**
     * Initializes the action event for the Connect-Button btnConnect.
     */
    private void setClickListener() {
        //if (ip != null){
        //  Log.i("App_status", "Connecting ...");
        btnConnect.setOnClickListener((view) -> changeConnect());//}
    }

    /**
     * Case 1: Disconnected
     * Connects with the broker.
     * Login: raspi , Pass: RhabarberPi
     * Constants.Broker_IP = IP from broker device  - using port :1883.
     * Subscribes to Constants.TOPIC
     * <p>
     * Case 2: Connected
     * Unsubscribes from topic and disconnects from broker.
     */
    public void changeConnect() {

        Intent intent = new Intent(this, MQTTService.class);
        intent.putExtra("local.oems_g5_projekt.task", "connect");
        intent.putExtra("local.oems_g5_projekt.ip", ipInput.getText().toString());
        startService(intent);

        intent = new Intent(this, MQTTService.class);
        intent.putExtra("local.oems_g5_projekt.task", "subscribe");
        intent.putExtra("local.oems_g5_projekt.topic", Constants.TOPIC_temp);
        startService(intent);

        // TODO 4) add your topic subscription
    }

    private boolean checkingBoolean(String message) {
        if (message.equals("1"))
            return true;
        else
            return false;
    }

    @Override
    public void updateClient(String topic, String message) {
        System.out.println("updating client: " + topic + " : " + message);
        switch (topic) {
            // TODO 5) still not compiling? did you add correct lines to Constants.java file?
            // What else is missing for your LED to work?
            case Constants.TOPIC_TODO:
                chkLed1.setChecked(checkingBoolean(message));
                break;

            case Constants.TOPIC_temp:
                if (chkTemp.isChecked())
                    txtTempOut.setText(message + " °C");
                break;
        }
    }

    /**
     * Changes the LED status on checkbox input and persist its value
     *
     * @param chk
     * @param img
     */
    private void setChangedListener(CheckBox chk, String saveAs, ImageView img) {
        chk.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            changeLedStatus(isChecked, img);
            setCheckBoxSettings(saveAs, chk);

            // TODO 6) if you don't publish back to the topic, you cannot turn your lamps off/on ;)
            if (chk == chkLed1) {
                if (chk.isChecked())
                    mqttService.publish(Constants.TOPIC_TODO, "1");
                else
                    mqttService.publish(Constants.TOPIC_TODO, "0");
            }

        }));
    }

    /**
     * Set a changed listener for the specified checkbox that persist its value
     *
     * @param chk
     * @param saveAs
     */
    private void setChangedListener(CheckBox chk, String saveAs) {
        chk.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            setCheckBoxSettings(saveAs, chk);
        }));
    }
}
