package local.oems_g5_projekt;

public class Constants {

    // TODO 1) enter correct broker IP (ask instructor if its going to work)
    public static final String BROKER_IP = "192.168.178.20";
    public static final String TOPIC_temp = "Home/Temp/State";
    // TODO 2) add your  topic (Home/LED/x
    public static final String TOPIC_TODO = "T/o/D/o";

    public static final int QOS_0 = 0;
    public static final int QOS_1 = 1;
    public static final int QOS_2 = 2;
}
