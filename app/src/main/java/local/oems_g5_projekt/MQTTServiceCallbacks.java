package local.oems_g5_projekt;

/**
 * Created by Steve on 19.05.2017.
 */

public interface MQTTServiceCallbacks {
    void updateClient(String topic, String message);
}
