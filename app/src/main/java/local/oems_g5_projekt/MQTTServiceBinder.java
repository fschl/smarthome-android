package local.oems_g5_projekt;

import android.os.Binder;

public class MQTTServiceBinder extends Binder {
    private MQTTService mqttService;

    public MQTTServiceBinder(MQTTService mqttService){
        this.mqttService = mqttService;
    }

    public MQTTService getService(){
        return mqttService;
    }
}
