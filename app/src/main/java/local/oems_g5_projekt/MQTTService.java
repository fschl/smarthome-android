package local.oems_g5_projekt;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import static org.eclipse.paho.client.mqttv3.MqttAsyncClient.generateClientId;

public class MQTTService extends Service {
    Handler handler = new Handler(Looper.getMainLooper());
    byte[] messageBytes;
    int qos = 2;
    boolean retain = true;
    private MqttClient mqttClient;
    private MqttMessage outMessage;
    private IBinder mBinder;
    private MQTTServiceCallbacks activity;

    public MQTTService() {
        mBinder = new MQTTServiceBinder(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void registerClient(Activity activity) {
        this.activity = (MQTTServiceCallbacks) activity;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getStringExtra("local.oems_g5_projekt.task")) {
            case "connect":
                connect(intent.getStringExtra("local.oems_g5_projekt.ip"));
                break;
            case "publish":
                publish(intent.getStringExtra("local.oems_g5_projekt.topic"), intent.getStringExtra("message"));
                break;
            case "subscribe":
                subscribe(intent.getStringExtra("local.oems_g5_projekt.topic"));
                break;
        }
        return Service.START_NOT_STICKY;
    }

    private void unsubscribe(String topic) {
        try {
            mqttClient.unsubscribe(topic);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void subscribe(String topic) {
        try {
            mqttClient.subscribe(topic, 2);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Verbindet sich mit dem Broker mit der angegebenen IP Adressee
     *
     * @param ip
     */
    private void connect(String ip) {
        try {
            // Trennt sich vom Broker wenn Client bereits verbunden
            if (mqttClient != null) mqttClient.disconnect();


            // Erstellt einen Mqtt Client, der mit dem Server/Broker kommunizieren kann
            mqttClient = new MqttClient("tcp://" + ip + ":1883", "test-dev", new MemoryPersistence());

            // Legt fest, wie der MQTTService auf bestimmte Ereignisse reagiert
            mqttClient.setCallback(new MqttCallback() {

                // Wird aufgerufen wenn die Verbindung abbricht
                @Override
                public void connectionLost(Throwable throwable) {
                    //Toast.makeText(MQTTService.this, "Verbindung verloren...", Toast.LENGTH_LONG).show();
                }

                // Wird aufgerufen wenn eine Message vom Broker eintrifft
                @Override
                public void messageArrived(String t, MqttMessage m) throws Exception {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            String message = new String(m.getPayload());
                            System.out.println(t + ":: " + m.toString() + ": " + message);
                            if (!t.contains("/set")) {
                                activity.updateClient(t, message);
                            }
                        }
                    });
                }

                // Wird aufgerufen wenn eine Message an den Broker geschickt wurde.
                @Override
                public void deliveryComplete(IMqttDeliveryToken t) {
                }
            });

            // Verbindet en Client mit dem Server/Broker
            mqttClient.connect();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MQTTService.this, "Verbunden", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void publish(String topic, String message) {
        try {
            System.out.println("setting " + topic + ": " + message);
            mqttClient.publish(topic + "/set", (message).getBytes(), qos, retain);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
